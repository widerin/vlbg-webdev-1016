

def test_ping_success_via_get(client):
    response = client.get('/ping')
    assert 200 == response.status_code


def test_ping_failure_via_post(client):
    response = client.post('/ping')
    assert 405 == response.status_code


def test_ping_answer_is_pong(client):
    response = client.get('/ping')
    assert 200 == response.status_code
    assert 'Pong!' in str(response.data)
