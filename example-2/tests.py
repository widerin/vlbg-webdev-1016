import pytest


def test_ping_success_via_get(client):
    response = client.get('/ping')
    assert 200 == response.status_code


def test_ping_failure_via_post(client):
    response = client.post('/ping')
    assert 405 == response.status_code


def test_ping_answer_is_pong(client):
    response = client.get('/ping')
    assert 200 == response.status_code
    assert 'Pong!' in str(response.data)


@pytest.fixture
def empty_redis():
    from server import redis_store
    redis_store.set('test', 0)


@pytest.mark.parametrize('requests,pings', [
    (1, 1),
    (2, 2),
    (10, 10),
])
def test_ping_answer_is_pong_with_counter(client, empty_redis, requests, pings):  # NOQA
    for i in range(0, requests):
        response = client.get('/ping')
    assert 200 == response.status_code
    assert '%d pings' % (pings) in str(response.data)
