from flask import Flask
from flask import request
from flask_redis import FlaskRedis
import os

app = Flask(__name__)

REDIS_URL = os.environ.get('REDIS_URL', os.environ.get('REDIS_PORT'))
if REDIS_URL:
    app.config.update({'REDIS_URL': REDIS_URL})
redis_store = FlaskRedis(app)


@app.route('/ping')
def ping_advanced():
    key = request.environ.get('REMOTE_ADDR', 'test')
    try:
        value = int(redis_store.get(key)) + 1
    except TypeError:
        value = 1

    redis_store.set(key, value)
    return '<h1>Pong!</h1><h2>You sent %d pings</h2>' % value
