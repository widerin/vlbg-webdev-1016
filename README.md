# VLBG WEBDEV Oct. 2016

Sample Flask application dockerized

* [example-1](./example-1) - Flask sample ping application
* [example-2](./example-2) - Flask sample ping application with connected redis to count pings per remote ip.

## Install

    git clone https://gitlab.com/widerin/vlbg-webdev-1016.git
    cd vlbg-webdev-1016
    virtualenv -p python3 .
    source bin/activate

### Example 1

    cd example-1
    pip install -r requirements.txt
    FLASK_APP=server.py flask run

or

    docker run -p 5000:5000 registry.gitlab.com/widerin/vlbg-webdev-1016:example-1

Run application in browser using http://localhost:5000/ping

### Example 2

This app requires a running redis server, you can start on using docker:

    docker run -d --name redis redis

Now you're ready to launch example 2

    cd example-2
    pip install -r requirements.txt
    FLASK_APP=server.py flask run
    
or 

    docker run -p 5000:5000 --link redis:redis registry.gitlab.com/widerin/vlbg-webdev-1016:example-2

Run application in browser using http://localhost:5000/ping. Each time you
refresh your browser, it will increment the counter stored in Redis:

    REMOTE_ADDR => value++

